Proyecto de prueba para Napptilus

Creado en Node.js v18.16.0 y npm v9.5.1
con create-react-app

## Scripts disponibles

### `npm start`

Corre la aplicación en modo desarrollo.

### `npm test`

Corre los tests unitarios.

### `npm run build`

Crea la carpeta build con los archivos necesarios para desplegar la aplicación en producción.

### `npm run lint`

Corre el linter para verificar que el código cumple con las reglas de estilo definidas en el archivo .eslintrc.js
