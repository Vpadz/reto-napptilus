module.exports = {
  extends: 'airbnb',
  globals: {
    localStorage: true,
    fetch: true,
  },
  rules: {
    'react/prop-types': 'off',
  },
};
