import { Card } from '@mui/material';
import React from 'react';

export default function ItemImage({ imgsrc, alt }) {
  return (
    <Card className="product-image">
      <img src={imgsrc} alt={alt} />
    </Card>
  );
}
