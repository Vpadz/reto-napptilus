import React from 'react';
import { Button, Card } from '@mui/material';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

export default function ItemActions({ colors, storages, id }) {
  return (
    <Card className="product-actions" data-product-id={id}>
      <div className="product-colors">
        {colors.length > 0
          ? (
            <FormControl>
              <FormLabel id="color-group">Color:</FormLabel>
              <RadioGroup
                row
                aria-labelledby="color-group"
                name="color-radios-group"
                defaultValue={colors[0].code}
              >
                {colors.map((color) => (
                  <FormControlLabel
                    key={color.code}
                    value={color.code}
                    control={<Radio />}
                    label={color.name}
                  />
                ))}
              </RadioGroup>
            </FormControl>
          ) : ''}
      </div>
      <div className="product-storages">
        {colors.length > 0
          ? (
            <FormControl>
              <FormLabel id="storage-group">Almacenamiento:</FormLabel>
              <RadioGroup
                row
                aria-labelledby="storage-group"
                name="storage-radios-group"
                defaultValue={storages[0].code}
              >
                {storages.map((storage) => (
                  <FormControlLabel
                    key={storage.code}
                    value={storage.code}
                    control={<Radio />}
                    label={storage.name}
                  />
                ))}
              </RadioGroup>
            </FormControl>
          ) : ''}
      </div>
      <div>
        <Button variant="contained">AÑADIR AL CARRITO</Button>
      </div>
    </Card>
  );
}
