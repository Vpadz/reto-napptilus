/* eslint-disable react/prop-types */
import { Card } from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';

export default function Item({ data }) {
  const {
    model, price, imgUrl, id,
  } = data;

  return (
    <Link to={`/product/${id}`} className="item-container">
      <Card className="item">
        <div className="item-image">
          <img src={imgUrl} alt="product" />
        </div>
        <p>{model}</p>
        <p>{price || 'No disponible'}</p>
      </Card>
    </Link>
  );
}
