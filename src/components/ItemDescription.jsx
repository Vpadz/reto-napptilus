import { Card } from '@mui/material';
import React from 'react';

export default function ItemDescription({ data }) {
  return (
    <Card className="product-description">
      {/* <p>{data.id}</p> */}
      <p>
        MARCA:
        {' '}
        {data.brand}
      </p>
      <p>
        MODELO:
        {' '}
        {data.model}
      </p>
      <p>
        PRECIO:
        {' '}
        {data.price}
      </p>
      <p>
        CPU:
        {' '}
        {data.cpu}
      </p>
      <p>
        RAM:
        {' '}
        {data.ram}
      </p>
      <p>
        SISTEMA OPERATIVO:
        {data.os}
      </p>
      <p>
        RESOLUCION:
        {' '}
        {data.displayresolution}
      </p>
      <p>
        BATERIA:
        {' '}
        {data.battery}
      </p>
      <p>
        CAMARA PRINCIPAL
        {data.primaryCamera}
      </p>
      <p>
        CAMARA SECUNDARIA:
        {' '}
        {data.secondaryCamera}
      </p>
      <p>
        DIMENSIONES:
        {' '}
        {data.dimensions}
      </p>
      <p>
        PESO:
        {' '}
        {data.weight}
      </p>
    </Card>
  );
}
// {id, brand, model, price, imgUrl, networkTechnology, networkSpeed,
//   gprs, edge, announced, status, dimentions, weight, sim,
//   displayType, displayResolution, displaySize, os, cpu,
//   chipset, gpu, externalMemory, internalMemory, ram, primaryCamera,
//   secondaryCmera, speaker, audioJack, wlan, bluetooth, gps,
//   nfc, radio, usb, sensors, battery, colors, options}
