import { TextField } from '@mui/material';
import React from 'react';
// import { Link } from 'react-router-dom';

export default function Header() {
  return (
    <div className="product-searchbar">
      <TextField id="searchBar" label="Buscador..." variant="outlined" className="searchbar" />
    </div>
  );
}
