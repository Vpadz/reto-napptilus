import React from 'react';
// import { Link } from 'react-router-dom';

export default function Header() {
  return (
    <header className="navbar">
      {/* <Link to="/"> */}
      <h1>Reto Napptilus</h1>
      {/* </Link> */}
      <span>Breadcrumbs</span>
      <span>Carrito (0)</span>
    </header>
  );
}
