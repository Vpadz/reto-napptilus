// import React from 'react';
import React, { useState, useEffect } from 'react';
import { useLoaderData } from 'react-router-dom';
import ItemDescription from '../components/ItemDescription';
import ItemActions from '../components/ItemActions';
import ItemImage from '../components/ItemImage';
// import Item from '../components/Item';

export async function loader({ params }) {
  const product = params.productId;
  return { product };
}

export default function ProductDetailsPage() {
  const { product } = useLoaderData();
  const [data, setData] = useState([]);
  const url = `https://itx-frontend-test.onrender.com/api/product/${product}`;
  const fetchProduct = () => fetch(url)
    .then((res) => res.json())
    .then((d) => setData(d));
  useEffect(() => {
    fetchProduct();
  }, []);
  // console.log(data.options.colors);
  return (
    <div className="product-container">
      <div className="product-image-container">
        <ItemImage imgsrc={data.imgUrl} />
      </div>
      <div className="product-info">
        <ItemDescription data={data} />
        <ItemActions
          colors={data.options && data.options.colors ? data.options.colors : []}
          storages={data.options && data.options.storages ? data.options.storages : []}
          productId={data.id}
        />
      </div>
    </div>
  );
}
