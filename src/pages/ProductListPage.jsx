import React, { useState, useEffect } from 'react';
import Item from '../components/Item';
import ListSearch from '../components/ListSearch';

export default function ProductListPage() {
  const [data, setData] = useState([]);
  const url = 'https://itx-frontend-test.onrender.com/api/product';
  const fetchInfo = () => fetch(url).then((res) => res.json()).then((d) => {
    setData(d);
    localStorage.setItem('data', JSON.stringify(d));
    localStorage.setItem('data-date', JSON.stringify(Date.now()));
  });

  useEffect(() => {
    const currentDate = Date.now();
    if (currentDate - localStorage.getItem('data-date') > 3600000) {
      fetchInfo();
    } else {
      setData((JSON.parse(localStorage.getItem('data'))));
    }
  }, []);

  return (
    <div className="products-list-container">
      <ListSearch />
      <div className="products-list">
        {data.map((item) => (
          <Item key={item.id} data={item} />
        ))}
      </div>
    </div>
  );
}
