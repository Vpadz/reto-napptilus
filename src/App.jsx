import React from 'react';
// import logo from './logo.svg';
import {
  createBrowserRouter,
  RouterProvider,
} from 'react-router-dom';
import ProductListPage from './pages/ProductListPage';
import Header from './components/Header';
import ProductDetailsPage, { loader as pdpLoader } from './pages/ProductDetailsPage';
import './App.css';

const router = createBrowserRouter([
  {
    path: '/',
    element: <ProductListPage />,
  },
  {
    path: '/product/:productId',
    element: <ProductDetailsPage />,
    loader: pdpLoader,
  },
]);

function App() {
  return (
    <div className="App">
      <Header />
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
